# This file is a library to be imported in support of the archive and version
# management programs.

import os
import os.path
import re
import sys
import json
import argparse
import subprocess
import shutil
import filecmp
from pprint import pprint


# Constants
PRE = 'buoy-install-'
POST = '.tar.gz'

# Variables

# Client management file and location.
file_mgmt_db=''
dir_mgmt_db=''

# Downloadable archive directory - default
dir_archive=''

# Fulfillment servers & URI
host_archive = []
uri_upgrade=''

# Upgrade configuration file.  The key to the universe.
CONFIG_UPGRADE='/etc/murtech/buoy/client/upgrade.cfg'


####################
# Back up the current management file
def backup_mgmt_file():
        print('Backing up the management database...')
        listing = os.listdir(dir_mgmt_db)
        # Backups will be of the form access.json.n.bak where n is an incrementing integer.
        listing.remove(file_mgmt_db)
        if not len(listing):
                name_backup = file_mgmt_db + '.1.bak'
        else:
                high_num = 0
                for backup in listing:
                        num = int(backup.split('.')[2])
                        if num > high_num:
                                high_num = num
                name_backup = file_mgmt_db + '.' + str(high_num + 1) + '.bak'

        path_src = dir_mgmt_db + '/' + file_mgmt_db
        path_dest = dir_mgmt_db + '/' + name_backup
        shutil.copy2(path_src, path_dest)


####################
# Return a sorted list of items based on end of string qualifier.
def qualify_list_items(qualifier, file_list):
        list_validated = []

        for item in file_list:
                if item.endswith(qualifier):
                        list_validated.append(item)

        list_validated.sort()
        return list_validated


####################
# Compare backup files in management database.
def cmp_backups(dir_mgmt_db, f1, f2):
        path1 = dir_mgmt_db + '/' + f1
        path2 = dir_mgmt_db + '/' + f2

        return filecmp.cmp(path1, path2, shallow=False)


####################
# Explain contents of the given version.
def explain_version(db_mgmt, args):
	with open(db_mgmt, 'r') as f:
		mgmt = json.load(f)

	section = ['prod', 'dev']

	for version in args:
		for sect in section:
			keys = list(mgmt[sect].keys())
			if version in keys:
				pprint(mgmt)


####################
# Delete redundant backups.
def del_backups_redundant(db_mgmt):
        listing = os.listdir(dir_mgmt_db)
        list_bak = qualify_list_items('.bak', listing)

        list_deletion = []

        if len(list_bak) > 1:
                while len(list_bak):
                        compare_to = list_bak.pop(0)
                        for compare_at in list_bak:
                                if cmp_backups(dir_mgmt_db, compare_to, compare_at):
                                        list_deletion.append(compare_at)

        for item in list_deletion:
                os.remove(dir_mgmt_db + '/' + item)


####################
# List the archives currently under management.
def list_managed(db_mgmt):
        if not os.path.isfile(db_mgmt):
                print('Management file [ ' + db_mgmt + ' ] doesn\'t exist...exiting...', file=std_err)
                sys.exit(1)

        # Get independent lists for production and development.
        with open(db_mgmt, 'r') as f:
                mgmt = json.load(f)

        dev = list(mgmt['dev'].keys())
        prod = list(mgmt['prod'].keys())

        print('Production versions under management -')
        if not len(prod):
                print('    - There are no production versions under management.')
        else:
                for item in prod:
                        print('    - ' + PRE + item + POST)

        print('Development versions under management -')
        if not len(dev):
                print('    - There are no development versions under management.')
        else:
                for item in dev:
                        print('    - ' + PRE + item + POST)

        print()



####################
# Qualify a string of version numbers.
def qual_version(list_version):

	valid = True

	for ver in list_version:
		if re.match(r'[0-9]*.[0-9]*.[0-9]*-[0-9]*', ver):
			continue

		valid = False

	return valid


############################
# Processs options
def proc_options(args):
	options = {}

	devops = 'prod'
	if args.dev:
		devops = 'dev'
	options['devops'] = devops

	permit = 'allow'
	if args.deny:
		permit = 'deny'
	options['permit'] = permit

	return options



####################################
# Parse config file for the goodies.
with open(CONFIG_UPGRADE, 'rt') as cfg:
        for line in cfg:
                if line.startswith('DIR_ARCHIVE'):
                        components=re.split(r'[=\s]\s*', line)
                        dir_archive=components[1]
                if line.startswith('ACCESS_MGMT'):
                        components=re.split(r'[=\s]\s*', line)
                        db_mgmt = components[1]
                        file_mgmt_db=os.path.split(db_mgmt)[1]
                        dir_mgmt_db = os.path.split(db_mgmt)[0]
                if line.startswith('HOST_ARCHIVE'):
                        components=re.split(r'[=\s]\s*', line)
                        host_archive.append(components[1])
                if line.startswith('URI_ARCHIVE'):
                        components=re.split(r'[=\s]\s*', line)
                        uri_upgrade = components[1]


