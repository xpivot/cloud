package us.murtech.buoy.embedded.upgrade.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "UpgradeServlet",
        description = "Upgrade request and fulfillment for Omega2+ units.",
        urlPatterns = {"/upgrade_req"}
)
public class UpgradeServlet extends HttpServlet {

    final static String HDR_BUOY_ID = "Buoy-Id";
    final static String HDR_BUOY_VERSION = "Buoy-Version";

    // Request info will be contained in the headers.
    protected void doGet(
            HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        final Logger logger = LogManager.getLogger(this.getClass().getName());

        // Both of these headers must exist, or the request is invalid.
        String buoy_id = request.getHeader(HDR_BUOY_ID);
        String buoy_version = request.getHeader(HDR_BUOY_VERSION);

        if(buoy_id != null && buoy_version != null) {
            String msg = "GET with non-null required headers.";
            System.out.println("Println() - remove when logging works - " + msg);
            logger.debug(msg);
            String headers = "Buoy ID [ " + buoy_id + " ] -- Software Version [ " + buoy_version + " ]";
            System.out.println(headers);
            logger.debug(headers);
            response.setStatus(200);

        }
        else {
            String errMsg = "GET request with one or both of the required headers was/were null.";
            System.out.println(errMsg);
            logger.error(errMsg);
            response.setHeader("Buoy-Headers", "Missing");
            response.setHeader(HDR_BUOY_ID, buoy_id);
            response.setHeader(HDR_BUOY_VERSION, buoy_version);
            response.sendError(HttpServletResponse.SC_FORBIDDEN,
                    "Required header Buoy-Id and/or Buoy-Version is null.");
        }
    }
}
