#! /usr/bin/python3
#
# Find the Omega2+ devices listed in dhcpd.conf
#
import subprocess
import re
import os
from collections import OrderedDict

# Find all the IP addresses of the Omega2 devices in the DHCP config file.
command = ['grep', '-A 2', 'omega', '/etc/dhcpd.conf']
dev_dhcp = subprocess.check_output(command).decode('utf-8')
dev_dhcp = re.split(r'[{;\n\t\s]\s*', dev_dhcp)
count = dev_dhcp.count('host')
#device_ip = {}
device_ip = OrderedDict({}) 
for x in range(0, count):
	device_ip[dev_dhcp[x * 9 + 1]] = dev_dhcp[x * 9 + 7]

# For each device configured in DHCP, ping the IP address.  Skip over the 
# "foo" placeholders.  Sort by IP address (OrderedDict() does this).
omegas = list(device_ip.keys())
for omega in omegas:
	# Something broke above, apparently in the preceeding for loop.  There is
	# no guarantee the devices will appear in the disctionary in the order of
	# appearance in the dhcpd.conf file.  As such the 'brek' below will terminate
	# this for loop prior to finding all of the devices.  Change to 'continue'.
	if omega.find('foo') >= 0:
		#break
		continue

	ip = device_ip[omega]
	resp = os.system("ping -c 1 " + ip + " > /dev/null 2>&1")
	if resp == 0:
		print("Device [ " + omega + " ] is on line at IP_ADDR  [ " + ip + " ]")
	else:
		print("Device [ " + omega + " ] is off line at IP_ADDR [ " + ip + " ]")



